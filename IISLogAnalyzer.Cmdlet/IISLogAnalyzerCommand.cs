﻿using System.IO;
using System.Management.Automation;

namespace IISLogAnalyzer.Cmdlet
{
    [Cmdlet(VerbsLifecycle.Invoke, "AnalyzeDirectory")]
    public class IISLogAnalyzerCommand : System.Management.Automation.Cmdlet
    {
        protected override void ProcessRecord()
        {
            SessionState state = new SessionState();
            Directory.SetCurrentDirectory(state.Path.CurrentFileSystemLocation.Path);

            var directory = Directory.GetCurrentDirectory();
            this.WriteObject($"Inicializadon procesado de directorio: {directory}");

            var documents = IISLogAnalyzer.GetParsedDocuments(directory);
        }
    }
}
