﻿using IISLogAnalyzer.Database;
using IISLogAnalyzer.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace IISLogAnalyzer.Tests
{
    [TestClass]
    public class IISLogAnalyzerTest
    {
        [TestMethod]
        public void TestParseLog()
        {
            // Ruta base para directorio de logs.
            string path = Path.GetFullPath(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "..\\Sources"));

            IEnumerable<ParsedDocument> parsedDocuments = IISLogAnalyzer.GetParsedDocuments(path);
            Assert.IsNotNull(parsedDocuments.Count());

            var handler = new DatabaseHandler("Server=127.0.0.1,1433;User Id=SA;Password=sa;Integrated Security=false", "Pruebas1");

            foreach (ParsedDocument document in parsedDocuments)
            {
                Assert.IsTrue(document.Persist(handler));
            }
        }

        [TestMethod]
        public void TestDatabase()
        {
            var handler = new DatabaseHandler("Server=127.0.0.1,1433;User Id=SA;Password=sa;Integrated Security=false", "Test");

            handler.CreateDatabase();
        }
    }
}
