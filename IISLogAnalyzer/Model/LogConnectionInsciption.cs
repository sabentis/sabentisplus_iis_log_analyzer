﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IISLogAnalyzer.Model
{
    public class LogConnectionInsciption
    {
        public Guid ID { get; set; }
        public int InscriptionId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
