﻿using IISLogAnalyzer.Database;

namespace IISLogAnalyzer.Model
{
    internal interface IPersistible
    {
        bool Persist(DatabaseHandler handler);
    }
}
