﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Text;

namespace IISLogAnalyzer.Database
{
    public class DatabaseHandler
    {
        public DatabaseHandler(string connectionString, string databasename)
        {
            this.ConnectionStringBuilder = new SqlConnectionStringBuilder(connectionString);
            this.DataBaseName = databasename;
        }

        /// <summary>
        /// Gets MSSQL ConnectionString
        /// </summary>
        public SqlConnectionStringBuilder ConnectionStringBuilder { get; }

        /// <summary>
        /// Gets or sets database Name
        /// </summary>
        public string DataBaseName { get; }

        public void CreateDatabase()
        {
            using (var connection = new SqlConnection(this.ConnectionStringBuilder.ConnectionString))
            {
                connection.Open();

                try
                {
                    this.Command(string.Format("IF DB_ID('{0}') IS NULL CREATE DATABASE [{0}]", this.DataBaseName), connection).ExecuteNonQuery();
                    connection.ChangeDatabase(this.DataBaseName);

                    if (!this.TableExists("LOGCONNECTION_INSCRIPTION", connection))
                    {
                        var resource = this.GetResource(typeof(DatabaseHandler).Assembly, "IISLogAnalyzer.Scripts.LOGCONNECTION_INSCRIPTION.sql");
                        var res = this.Command(string.Format(resource, this.DataBaseName), connection).ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    connection.Close();
                    throw e;
                }
            }
        }

        public string GetResource(Assembly assembly, string path)
        {
            var stream = assembly.GetManifestResourceStream(path);

            using (TextReader tx = new StreamReader(stream, Encoding.UTF8, true))
            {
                return tx.ReadToEnd();
            }

            throw new Exception($"Resource not found in binary '{assembly.FullName}'");
        }

        public SqlCommand Command(string query, SqlConnection connection, SqlTransaction trans = null)
        {
            if (trans == null)
            {
                return new SqlCommand(query, connection);
            }

            return new SqlCommand(query, connection, trans);
        }

        private bool TableExists(string tableName, SqlConnection connection)
        {
            using (SqlCommand command = new SqlCommand("SELECT * FROM sys.tables", connection))
            {
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var name = reader.GetString(0).ToLower();
                    if (name == tableName.ToLower())
                    {
                        return true;
                    }
                }

                reader.Close();
            }

            return false;
        }
    }
}
